Installation
============

Required dependencies
---------------------

* Python 2.7, 3.4, 3.5, or 3.6
* `numpy <http://www.numpy.org/>`__ (1.7 or later)
* `pandas <http://pandas.pydata.org/>`__ (0.15.0 or later)
* `xarray <http://xarray.pydata.org/>`__ (0.9.0 or later)


Instructions
------------

Using conda
~~~~~~~~~~~

pyatran itself is a pure Python package, but its dependencies are not. The
easiest way to get everything installed is to use conda_. To install pyatran
using the conda command line tool:

.. code:: shell

   $ conda install -c andreas-h pyatran

.. _conda: http://conda.io/


Using pip
~~~~~~~~~

If you don’t use conda, be sure you have the required dependencies (see above)
installed first. Then, install pyatran with pip:

.. code:: shell

   $ pip install pyatran
