===================
pyatran development
===================

Repository
==========

The pyatran repository lives at `GitLab
<https://gitlab.com/andreas-h/pyatran>`__.  Source code is managed using `Git
<https://git-scm.com/>`__ version control.  pyatran adopts the `branching model
by Vincent Driessen
<http://nvie.com/posts/a-successful-git-branching-model/>`__.


Setting up the development environment
======================================

First, you'll have to login to GitLab and create a fork of the pyatran
repository.

The simplest way to set up a development environment is via conda_:

.. code:: shell

   $ git clone git@gitlab.com:YOURUSERNAME/pyatran.git
   $ cd pyatran
   $ conda create -n pyatran_dev --file requirements_dev.txt

Now, whenever you want to work on *pyatran*, you simply have to activate this
environment:

.. code:: shell

   $ source activate pyatran_dev

.. _conda: https://docs.continuum.io/docs_oss/conda/


Packaging
=========

pyatran has been uploaded to `PyPI <https://pypi.python.org/pypi/pyatran/>`__.


Version numbers
===============

pyatran uses versioneer_ for managing version numbers.  This means that it is
*never* necessary to edit any version numbers in any files, as the version is
determined automatically from the state of the version control system.

.. _versioneer: https://github.com/warner/python-versioneer


Testing
=======

pyatran uses `pytest <https://docs.pytest.org/>`__ for testing.


Making a release
================

1. Branch ``release-x.y.z`` off ``develop``:

   .. code:: shell

      $ git checkout develop
      $ git checkout -b release-x.y.z

2. Make last changes to ``release-x.y.z``.  At least, you should replace the
   *Unreleased* heading in ``docs/changelog.rst`` with the release version and
   date.

3. Merge the ``release-x.y.z`` branch into ``master``:

   .. code:: shell

      $ git checkout master
      $ git merge --no-ff release-x.y.z

4. Create a tag for the release:

   .. code:: shell

      $ git tag -a vx.y.z

   You will be asked to enter a tag annotation.          

   .. note::

      The automatic build and upload of conda packages is triggered by any new
      tag in the repository.  Whenever a new tag is pushed to the main GitLab
      repository, the CI build will get the most recent release from PyPI, build
      this, and upload to anaconda.org.  Therefore it is important to only push
      the new tag to gitlab *after* uploading the build to PyPI.

5. Create the source distribution:

   .. code:: shell

      $ python setup.py sdist

6. Upload this source distribution to PyPI:

   .. code:: shell

      $ twine upload --repository pypi dist/pyatran-x-y-z.tar.gz

7. Push the release to GitLab:

   .. code:: shell

      $ git push origin master
      $ git push --tags

8. Merge the "last changes" (see point 2. above) back into ``develop``:

   .. code:: shell

      $ git checkout develop
      $ git merge --no-ff master
