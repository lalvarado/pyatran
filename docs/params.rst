======================================
SCIATRAN parameter values in *pyatran*
======================================

Internally, pyatran represents a set of SCIATRAN parameter values as
JSON_ struct.  An example can be found in the file sciaconf.json_ in
the project's root directory.

.. _JSON: https://www.json.org/
.. _sciaconf.json: https://gitlab.com/andreas-h/pyatran/blob/develop/sciaconf.json


Structure of the JSON configuration
-----------------------------------

Basically, the JSON is one single dictionary, with the top-level keys
being the SCIATRAN parameter file names, e.g., ``control.inp``.  There
are some exceptional (i.e., additional) optional top-level keys:

``base_directory``
    If this configuration key is present, any relative path in any
    SCIATRAN configuration file will be prepended with the value of
    ``base_directory`` from the JSON struct, i.e., ``base_directory``
    is the base to which all relative paths to input files are
    interpreted (see below).


``sciatran_md5``
    The `MD5 checksum`_ of the SCIATRAN executable to be run.  If this
    configuration key is present, the MD5 hash of the SCIATRAN
    executable (parameter ``scia_exec`` to
    :py:func:`pyatran.runtime.setup_runtime`) will be checked against
    it.  The purpose of this parameter is to ensure that a given
    SCIATRAN simulation actually uses the proper executable.
    Otherwise, a ``ValueError`` will be raised.

.. _MD5 checksum: https://en.wikipedia.org/wiki/Cryptographic_hash_function#MD5


How to specify paths to input files
-----------------------------------

File paths in SCIATRAN configuration files can be relative, absolute
with a tilde character, absolute without a tilde character.

In any path the user wants to set in any SCIATRAN configuration file,
the tilde character ``~`` can be used.  If the *optional* parameter
``Home directory`` from the parameter file ``control.inp`` is set,
then all occurrences of ``~`` will be replaced by its value.  If the
``Home directory`` parameter is *not* set, then all occurrences of
``~`` will be replaced by the user's ``$HOME`` directory (just as in
Unix-like shells).

Any relative path will be prepended with the value of the top-level
JSON configuration key ``base_directory`` (see above). If this key
is not present, the current working directory is prepended to all
relative paths.

Any absolute path is untouched.
